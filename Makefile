.phony: data clean


exec: tp1.o
	gcc -Wall -pedantic -std=c99  -o tp1 tp1.o

tp1.o: tp1.c
	gcc -Wall -pedantic -std=c99  -c tp1.c

test : 
	./tp1 -c $(CP) -i data -o resultat.txt

clean: 
	rm *.o
	rm tp1
	rm code.txt

data:
	wget -N https://www.github.com/guyfrancoeur/INF3135_H2019/raw/master/tp1/data.zip
	unzip -o data -d ./data

resultat: 
	git add resultat.txt
	git commit -m "MAKE RESULTAT"
	git push origin master

CP = `cat cp.txt`

