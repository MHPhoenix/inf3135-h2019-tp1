# Travail pratique 1

## Description

Le cours de construction et maintenance de logiciel (INF3135), qui se donne 
a l'Universite du Quebec a Montreal, a pour but de nous initier en
construction et maintenance de logiciel et au langage C.

Danc ce premier Travail pratique, nous alons lire un fichier en entree 
et recuperer un certain intervalle d'entiers naturels afin de decouvrir 
s'il y existe de nouveaux nombres parfaits.
Nous allons ensuite sortir un fichier resultant contanant tous les 
nombres parfaits trouves. ceux-ci seront affichés ligne par ligne.

## Auteur

Houefa Orphyse Peggy Merveille MONGBO (MONH08519906)

## Fonctionnement 

Le programme peut être lancé en ligne de commande avec au minimum les deux syntaxes suivantes :

> $ ./tp1 -c CODE_permanent -i nom_du_fichier_en_entree.ext -o fuchier_sortie.ext

> $ ./tp1 -c CODE_permanent < nom_du_fichier_en_entree.ext > fichier_sortie.ext

## Contenu du projet

> **Le projet contient les fichiers suivants :**

> - cp.txt: contient mon code permanent complet en majuscule.
> - tp1.c : contient le code source du projet (inf3132-h2019-tp1), ainsi que la fonction main.
> - README.md : contient la description du projet.
> - Makefile : supporte les appels make, make clean, make data, make test et make resultat.
> - .gitignore : contient les fichiers . et ce qu'on ce ne veut pas faire apparaitre dans git status. 

## References

> **Documentation :**

> - notes de cours
> - OpenClassRoom

## Statut

Fonctionnel
