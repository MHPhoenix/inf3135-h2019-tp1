#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void lecteurFichier (FILE * fichierALire, FILE * fichierAEcrire);
void calculNombreParfait (FILE * fichierSortie, long debut, long fin);

int main (int argc, char *argv[]) {

	FILE * fichierDEntre = stdin;
	FILE * fichierDeSorti = stdout;
	char nomFichierEntre [200];
	char nomFichierSortie [200];
	char codePermanent [13];
	int argument_c = 0;
	int argument_i = 0;
	int argument_o = 0;
	int i;

	for (i = 1; i < argc; i = i+2) {

        	if ( strcmp (argv [i], "-c") == 0) {

			//code permanent
			argument_c = 1;

			if (strlen (argv [i + 1]) == 12) {
				strcpy (codePermanent, argv [i + 1]);

			} else {
				exit (2);
			}

		} else if ( strcmp (argv [i], "-i") == 0) {

			//fichier d'entree
			argument_i = 1;

			if (strlen (argv [i + 1] ) != 0) {
				strcpy (nomFichierEntre, argv [i + 1]);
			 	fichierDEntre = fopen (nomFichierEntre, "r");
			}

		} else if ( strcmp (argv [i], "-o") == 0 ) {

			// fichier de sortie
			argument_o = 1;

			if (strlen (argv [i + 1]) != 0) {
				strcpy (nomFichierSortie, argv [i + 1]);
				fichierDeSorti = fopen (nomFichierSortie, "w");

				if (fichierDeSorti == NULL) {
					exit (6);
				}

		}

		} else if ( (strcmp (argv [i], "-c") != 0) || ( strcmp (argv [i], "-i") != 0)
                                || ( strcmp (argv [i], "-o") != 0) ) {
                        exit(3);
                }


		 if ( (argument_i == 0 && argument_o == 0 && argument_c == 0 ) ||
			(argument_i == 1 && argument_o == 1 && argument_c == 0 ) ) {

			fprintf(stderr, "Usage: %s <-c CODEpermanent> [-i fichier.in] [-o fichier.out] \n", argv[0]);

			exit(1);
		}

        }

	lecteurFichier (fichierDEntre, fichierDeSorti);

	if (argument_i == 1) {
		fclose (fichierDEntre);
	}

	if (argument_o == 1) {
                fclose (fichierDeSorti);
        }

	if (argument_c == 1) {

                FILE *fichierASortir = fopen ("code.txt", "w");
                fputs (codePermanent, fichierASortir);
                fclose (fichierASortir);
        }

	return 0;
}

void lecteurFichier (FILE * fichierALire, FILE * fichierAEcrire) {

	long valeurDebut = 0;
	long valeurFin = 0;
	long temporaire = 0;

	if (fichierALire != NULL) {

		//fscanf pour stocker les valeurs du fichier
		fscanf (fichierALire, "%ld  %ld", &valeurDebut, &valeurFin);

		if (valeurDebut > valeurFin) {

			temporaire = valeurDebut;
			valeurDebut = valeurFin;
			valeurFin = temporaire;
		}

		calculNombreParfait (fichierAEcrire, valeurDebut, valeurFin);

	} else {
		exit(5);
	}
}


void calculNombreParfait (FILE * fichierSortie ,long debut, long fin) {

	long i;

	if (debut < 0 || fin < 0) {
       		 exit(4);
         }


	//i va prendre les differentes valeurs de l'intervalle
	for (i = debut; i <= fin; i++) {

		//j est le diviseur
		long j;
		long somme = 0;

		for (j = 1; j < i; j++) {

			if (i % j == 0) {
				somme = somme + j;
			}
		}

		if (somme == i && somme != 0) {
			fprintf (fichierSortie,"%ld\n", i);
		}
	}
}
